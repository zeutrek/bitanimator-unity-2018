# BitAnimator Unity 2018.4.20f1

"BitAnimator" updated for the version of "Unity 2018.4.20f1".
This package includes:

*Leviant ScreenSpace Ubershader, updated for "Unity 2018".

*BitAnimator_v1.1.2, updated to "Unity 2018", fixed several minor bugs.

*PoiyomiToonV5.1.1.06, updated for "Unity 2018", some bugs need fixing. But it is fully functional.

*Odds Custom SDK v12, Unlimited SDK, was corrected to work smoothly with "BitAnimator".

--Dynamic Bone v1.2.1 coming soon, a bug should be fixed.--


Non-profit project, all rights belong to their respective creators, this is a package that brings together outdated tools as of the upload date.
